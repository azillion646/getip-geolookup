const express = require("express");
const app = express();
const { lookup } = require("geoip-lite");
const router = express.Router();

router.get("/ipLookup", (req, res) => {
  // ip address of the user
  const ip = req.headers["x-forwarded-for"] || req.connection.remoteAddress;
  const host = req.headers["host"];
  const userAgent = req.headers["user-agent"];
  console.log(req.headers);
  // location of the user
  const ipLookup = lookup(ip);
  // console.log(ipLookup);
  res.send({ ip, host, ipLookup, userAgent });
});

app.use("/", router);
app.listen(5000, () => console.log("server started!!"));
